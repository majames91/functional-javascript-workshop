/**
 *
 * Created by mjames on 24/09/2015.
 */

// a recursive function is a function which calls itself

function myReduce(array, callbackFn, acc) {
    if (array.length === 0) {
        return acc;
    }

    return myReduce(array.slice(1), callbackFn, callbackFn(acc, array[0]));
};

module.exports = myReduce;
