/**
 *
 * Created by mjames on 24/09/2015.
 */


// reduce reduces an array to a single value. the callback function takes an accumulator and the current state
// and modifies the accumulator, eventually returning it as the single value.

function countWords(words) {
    return words.reduce(function(accumulator, currWord) {
        if (accumulator[currWord]) {
            accumulator[currWord] += 1;
        } else {
            accumulator[currWord] = 1;
        }

        return accumulator;
    }, {});
};

module.exports = countWords;
