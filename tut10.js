/**
 *
 * Created by mjames on 25/09/2015.
 */

// Function.prototype.bind takes the context and any number of arguments
// to create a partially applied function

function logger(namespace) {
    return console.log.bind(console, namespace);
}

module.exports = logger;