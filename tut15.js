/**
 *
 * Created by mjames on 25/09/2015.
 */



function myLoadUsers(userIds, load, done) {
    var users = [];

    userIds.forEach(function(userId, index) {
        load(userId, function(user) {
            users[index] = user;
        });
    });

    done(users);
}

module.exports = myLoadUsers;
