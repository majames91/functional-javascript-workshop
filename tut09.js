/**
 *
 * Created by mjames on 25/09/2015.
 */

// each function prototype has an apply method.
// Apply takes the context and arguments for the function (given as an array)

function logger(namespace) {

    return function() {
       var args = [].slice.call(arguments);
       console.log.apply(null, [namespace].concat(args));
    }
};

module.exports = logger;
