/**
 * Created by mjames on 24/09/2015.
 */

function upperCase(input) {
    return input.toUpperCase();
};

module.exports = upperCase;
