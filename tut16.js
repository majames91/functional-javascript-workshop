/**
 *
 * Created by mjames on 25/09/2015.
 */


function getDependencies(dependencyTree, isDependencyLvl) {
    var dependencies = [];

    Object.keys(dependencyTree).forEach(function(key) {
        if (isDependencyLvl) {
            dependencies.push(key + '@' + dependencyTree[key].version);
        }

        if (key === 'dependencies')   {
            dependencies = dependencies.concat(getDependencies(dependencyTree[key], true));
        } else if (dependencyTree[key].dependencies) {
            dependencies = dependencies.concat(getDependencies(dependencyTree[key].dependencies, true));
        }
    });

    // remove duplicates and sort the array of dependencies
    return dependencies.filter(function(dependency, index) {
        return dependencies.indexOf(dependency) === index;
    }).sort();
}

var loremIpsum = {
    "name": "lorem-ipsum",
    "version": "0.1.1",
    "dependencies": {
        "optimist": {
            "version": "0.3.7",
            "dependencies": {
                "wordwrap": {
                    "version": "0.0.2"
                }
            }
        },
        "inflection": {
            "version": "1.2.6"
        }
    }
};


console.log(getDependencies(loremIpsum));

module.exports = getDependencies;