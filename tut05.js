/**
 * Created by mjames on 24/09/2015.
 */

// every returns true if the supplied callback returns true for every instance
// some returns true if the supplied callback returns true once or more

function checkUsersValid(validUsers) {
    return function(testUsers) {
      return testUsers.every(function(testUser) {
          return validUsers.some(function(validUser) {
              return validUser.id === testUser.id;
          });
      });
    };
};

module.exports = checkUsersValid;
