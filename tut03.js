/**
 * Created by mjames on 24/09/2015.
 */

// the map function calls the supplied function (passed in as the only argument) on each element in the array
// the returned value is stored in the returned array

function doubleAll(numbers) {
    return numbers.map(function(num) {
        return 2 * num;
    });
};

module.exports = doubleAll;
