/**
 *
 * Created by mjames on 25/09/2015.
 */

function repeat(fn, num) {
    var timer, count = 0;

    var newFn = function() {
        count += 1;
        fn();

        if (count >= num) {
            clearInterval(timer);
        }
    };

    timer = setInterval(newFn, 0);
}

module.exports = repeat;
