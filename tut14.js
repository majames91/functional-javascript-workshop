/**
 *
 * Created by mjames on 25/09/2015.
 */

function repeat(fn, num) {
    return function () {
        if (num <= 0) {
            return;
        }

        fn();
        return repeat(fn, num - 1);
    }
}

function trampoline(fn) {
    while(fn && typeof fn === 'function') {
        fn = fn();
    }

}

module.exports = function(fn, num) {
    trampoline(function() {
        return repeat(fn, num)
    });
};
