/**
 * Created by mjames on 25/09/2015.
 */


function myMap(array, callback) {
   return array.reduce(function (acc, curr) {
      acc.push(callback(curr));
      return acc;
   }, []);
};

module.exports = myMap;