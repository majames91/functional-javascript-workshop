/**
 * Created by mjames on 24/09/2015.
 */

// higher order functions are functions which take other functions in as arguments
// or the return type is function. Otherwise, the function is first order

function callFunctionNTimes(func, num) {
    if (num > 0) {
        func();
        callFunctionNTimes(func, num - 1);
    }
};

module.exports = callFunctionNTimes;