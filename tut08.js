/**
 *
 * Created by mjames on 25/09/2015.
 */


function countDucks() {
    var potentialDucks = [].slice.call(arguments);

    return potentialDucks.reduce(function(numDucks, potentialDuck) {
        var isDuck = Object.prototype.hasOwnProperty.call(potentialDuck, 'quack');
        return isDuck ? numDucks + 1 : numDucks;
    }, 0);
};

module.exports = countDucks;
