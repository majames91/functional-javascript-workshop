/**
 *
 * Created by mjames on 25/09/2015.
 */


function curryN(fn, n) {
    n = n || fn.length;


    return function(arg) {
        if (fn.length === 1) {
            return fn(arg);
        }

        fn = fn.bind(null, arg);
        return function(arg) {
            if (fn.length === 1) {
                return fn(arg);
            }

            fn = fn.bind(null, arg);
        }
    };
}

function add3(one, two, three) {
    return one + two + three;
}

var curryC = curryN(add3);
console.log(curryC);
var curryB = curryC(1);
console.log(curryB(2));
var curryA = curryB(2);
console.log(curryA);

module.exports = curryN;
