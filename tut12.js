/**
 *
 * Created by mjames on 25/09/2015.
 */


function Spy(obj, methodName) {
    var oldFn = obj[methodName];
    var spy = {
        count: 0
    };

    obj[methodName] = function () {
        var args = [].slice.call(arguments);

        spy.count += 1;
        return oldFn.apply(this, args);
    };

    return spy;
}

module.exports = Spy;
