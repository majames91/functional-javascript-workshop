/**
 *
 * Created by mjames on 24/09/2015.
 */


// filter returns an array of elements, where each element passed the test in the supplied callback function
// (pass the test === callback function returns true)

function getShortMessages(inputs) {
    return inputs.filter(function(input) {
      return input.message.length < 50;
    }).map(function(input) {
        return input.message;
    });
};

module.exports = getShortMessages;
